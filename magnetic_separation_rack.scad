
t = 4;
n_rows = 6;
space_side = 2.5;
space_magnet = 1;
adjust_magnet = -3;
space_between = space_magnet*2+t;
d_magnet = 12;
d_eppi = 11.25;
d_outer = d_eppi + 2*space_side;

w = t + 2*space_side + 2*space_magnet + 2*d_eppi;
l = (n_rows-1)*space_between + 2*space_side + n_rows * d_eppi;
spacing = d_eppi + space_between;

//l = 160;
//w = 43;
//w = 40;
h = 40;

initial_offset = space_side + d_eppi/2;

module tube() {
    translate([0, 0, -(18+17)]) union() {
        // total: 40.5mm
        // 39 without ball
        scale([1, 1, 1.5/(4.3/2)]) sphere(4.3/2);
        cylinder(h=17, d2=10.4, d1=4.3);
        translate([0, 0, 17])
        cylinder(h=18, d=10.4);
        translate([0, 0, 18+17])
        cylinder(h=2, d=13);
        scale([1.4, 1, 1])
        translate([0, 0, 18+17+2])
        cylinder(h=2, d=13);
    }
}

module tubes() {
    for (y = [initial_offset:spacing:l]) {
        translate([space_side+d_eppi/2, y, h+t])
        rotate([0, 0, 45]) tube();
    }
}

$fs=0.2;
$fa=0.2;
$fl=0.2;


module base_2d(peg_add=0.1) {
    difference() {
        hull() {
            for (y = [initial_offset:spacing:l]) {
                translate([space_side+d_eppi/2, y])
                circle(d=d_outer);
                translate([w-space_side-d_eppi/2, y])
                circle(d=d_outer);
            }
        }
        for (y = [(initial_offset+spacing/2):spacing:l]) {
            translate([space_side+d_eppi+space_magnet+t/2, y]) 
            square(t+peg_add, center=true);
        }
    }
}

module top_2d() {
    difference() {
        base_2d();
        for (y = [initial_offset:spacing:l]) {
            translate([space_side+d_eppi/2, y])
            circle(d=d_eppi);
            translate([w-space_side-d_eppi/2, y])
            circle(d=d_eppi);
        }
    }
}

module center_2d() {
    difference() {
        union() {
            translate([t, 0]) 
            square([h-t, l]);
            for (y = [(initial_offset+spacing/2):spacing:l]) {
                translate([t/2, y]) square(t, center=true);
                translate([h+t/2, y]) square(t, center=true);
            }
        }
        for (y = [initial_offset:spacing:l]) {
            translate([(h-t)/2+adjust_magnet+t, y])
                circle(d=d_magnet);
        }
    }
}

module base() {
    linear_extrude(height=t) base_2d();
}

module center() {
    linear_extrude(height=t) center_2d();
}

module top() {
    linear_extrude(height=t) top_2d();
}

module assembly() {
    base();
    translate([d_eppi + space_magnet + space_side + t, 0, 0]) rotate([0, -90, 0]) center();
    translate([0, 0, h]) top();
}

module assembly_2d() {
    top_2d();
    translate([w+t, 0, 0]) base_2d();
    translate([2*(w+t), 0, 0]) center_2d();
}




assembly_2d();
//assembly();
