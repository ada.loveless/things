// The diameter of the clockface
d_outer = 200;
// The diameter of the outer part of the markers
d_markers = d_outer * 0.9;
// The diameter of the hole in the center
d_stem = 10;

// The length of the big markers in mm
marker_l = d_outer * 0.2;
// The width of the big markers in degrees
marker_angle = 5;

// The length of small markers
small_marker_l = marker_l * 0.35;
// The width of small markers
small_marker_angle = 1;

// The hour hand length
hour_hand_length = (d_markers - marker_l)/2-small_marker_l;
// The minute hand rear length 
hour_hand_length_rear = hour_hand_length*0.3;
// The width at the base
hour_hand_width = 10;
// The width at the tip
hour_hand_width_tip = 7;

// The minute hand length
minute_hand_length = (d_markers-small_marker_l)/2+2;
// The minute hand rear length 
minute_hand_length_rear = minute_hand_length*0.2;
// The width at the base
minute_hand_width = 10;
// The width at the tip
minute_hand_width_tip = 5;

// Radius of the second hand blob
second_hand_r = 6;
// The second hand main length
second_hand_length = (d_markers - marker_l)/2-second_hand_r-2;
// The second hand back length
second_hand_length_rear = second_hand_length*0.2;
// The second hand width
second_hand_width = 3;

time = [10, 10, 36];

module marker(angle) {
    rotate(angle) intersection() {
        difference() {
            circle(d=d_markers);
            circle(d=d_markers-marker_l);
        }
        intersection() {
            rotate(marker_angle/2)
            square(d_outer/2);
            rotate(90-marker_angle/2)
            square(d_outer/2);
        }
    }
}

module marker_small(angle) {
    rotate(angle) intersection() {
        difference() {
            circle(d=d_markers);
            circle(d=d_markers-small_marker_l);
        }
        intersection() {
            rotate(small_marker_angle/2)
            square(d_outer/2);
            rotate(90-small_marker_angle/2)
            square(d_outer/2);
        }
    }
}

module hand(l1, l2, w1, w2) {
    path = [
        [w1/2, -l2],
        [-w1/2, -l2],
        [-w2/2, l1],
        [w2/2, l1],
    ];
    union() {
        polygon(path);
        circle(d=w1*1.5);
    }
}

module hour_hand(time=time) {
    //      Hour angle          Minutes addition  Seconds addition
    angle = -360*(time[0]/12) - 30*(time[1]/60) - 0.5*(time[2]/60);
    // The angle is negative because openscad does this backwards from how a
    // clock does it.
    rotate(angle) hand(hour_hand_length, hour_hand_length_rear, hour_hand_width, hour_hand_width_tip);
}

module minute_hand(time=time) {
    angle = -360*(time[1]/60) - 6*(time[2]/60);
    rotate(angle) hand(minute_hand_length, minute_hand_length_rear, minute_hand_width, minute_hand_width_tip);
}

module second_hand(time=time) {
    angle = -360*(time[2]/60);
    rotate(angle) union() {
        translate([-second_hand_width/2, -second_hand_length_rear])
            square([second_hand_width, second_hand_length+second_hand_length_rear]);
        translate([0, second_hand_length]) 
            circle(r=second_hand_r);
        circle(d=second_hand_width*2.0);
    }
}

module base() {
    difference() {
        circle(d=d_outer);
        circle(d=d_stem);
    }
}

base_material_height = 5;
top_material_height = 2;

module model(time) {
    color("white") linear_extrude(height=base_material_height) {
        base();
    }
    translate([0, 0, base_material_height]) {
        for (r = [0:30:330]) {
            color("black")
            linear_extrude(height=top_material_height) marker(r);
        }
        for (r = [0:6:360]) {
            color("black") linear_extrude(height=top_material_height) marker_small(r);
        }
        translate([0, 0, 1.5*top_material_height]) {
            color("black") linear_extrude(height=top_material_height) hour_hand(time);
            translate([0, 0, 1.5*top_material_height]) {
                color("black") linear_extrude(height=top_material_height) minute_hand(time);
                translate([0, 0, 1.5*top_material_height]) {
                    color("red") linear_extrude(height=top_material_height) second_hand(time);
                }
            }
        }
    }
}

function compute_hours(t) = t*12;
function compute_minutes(t) = (compute_hours(t)-floor(compute_hours(t)))*60;
function compute_seconds(t) = (compute_minutes(t)-floor(compute_minutes(t)))*60;
function compute_time(t) = [floor(compute_hours(t)), floor(compute_minutes(t)), floor(compute_seconds(t))];

model(compute_time($t));
